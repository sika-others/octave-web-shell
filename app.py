from computation_process import *

from flask import render_template, request, redirect, Flask
from flask.views import View

app = Flask(__name__)

app.debug = True

import tf

tmp = tf.TF()
cpt = tf.TF()
out = tf.TF()
err = tf.TF()

cpt.write("")

os.system("octave --no-line-editing -f computations > stdout")

@app.route("/")
def index_view():
    return open("index.html").read()

def compute(path):
    o1 = out.read()
    os.system("octave --no-line-editing -f %s > %s 2> %s"%(tmp.name, out.name, err.name))
    o = out.read()
    e = err.read()
    if e:
        return False, e
    return True, o.replace(o1, "")

@app.route("/api/0/compute/")#, allowed_method=["POST"])
def compute_view():
    cmd = request.args["cmd"]
    tmp.write(cpt.read()+cmd+"\n")
    status, res = compute(tmp.read())
    if status:
        cpt.append(cmd+"\n")
    return res

if __name__ == '__main__':
    app.run()
