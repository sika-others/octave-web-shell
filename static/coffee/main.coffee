Controller = ($scope, $http) ->
    document.getElementById('input').focus()
    $scope.out = ""
    $scope.lastIn = ""

    $scope.compute = () ->
        $scope.out = $scope.out + "octave> " + $scope.in + "\n"
        $http.get("/api/0/compute/?cmd="+$scope.in)
            .success (data) ->
                $scope.out = $scope.out + data
	        $scope.lastIn = $scope.in
	        $scope.in = ""
