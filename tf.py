import os

import tempfile

class TF:
    def __init__(self):
    	self.tf = tempfile.NamedTemporaryFile()
        self.tf.file.close()
        self.name = self.tf.name

    def close(self):
        self.tf.close()

    def tf_open(self, t):
        self.tf.file = open(self.tf.name, t)

    def write(self, data):
        self.tf_open("w")
        self.tf.file.write(data)
        self.tf.file.close()
        
    def append(self, data):
        self.tf_open("a")
        self.tf.file.write(data)
        self.tf.file.close()

    def read(self):
        self.tf_open("r")
        data = self.tf.file.read()
        self.tf.file.close()
	return data

def cpt_a(data):
    f = file("computations", "a")
    f.write(data)
    f.close()
def cpt_w(data):
    f = file("computations", "w")
    f.write(data)
    f.close()
def cpt_r():
    f = file("computations", "r")
    res = f.read()
    f.close()
    return res
def tmp_w(data):
    f = file("tmp", "w")
    f.write(data)
    f.close()
def tmp_r():
    f = file("tmp", "r")
    res = f.read()
    f.close()
    return res
def out_r():
    f = file("stdout", "r")
    res = f.read()
    f.close()
    return res
def err_r():
    f = file("stderr", "r")
    res = f.read()
    f.close()
    return res

def compute(path):
    o1 = out_r()
    os.system("octave --no-line-editing -f tmp > stdout 2> stderr")
    o = out_r()
    err = err_r()
    if err:
        return False, err
    return True, o.replace(o1, "")

if __name__ == '__main__':
    while True:
        try:
            cmd = raw_input()
        except KeyboardInterrupt:
            exit()
        tmp_w(cpt_r()+cmd+"\n")
        status, res = compute(tmp_r())
        print ">>> %s" % res
        if status:
            cpt_a(cmd+"\n")
